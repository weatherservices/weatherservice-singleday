package com.test.weather.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.*;

@Data  //WILL AUTO GENERATE THE GETTERS AND SETTERS - https://projectlombok.org/features/Data#:~:text=%40Data%20is%20a%20convenient%20shortcut,beans%3A%20getters%20for%20all%20fields%2C
/*@Data is like having implicit @Getter, @Setter, @ToString, @EqualsAndHashCode and @RequiredArgsConstructor annotations on the class (except that no constructor will be generated if any explicitly written constructors already exist). However, the parameters of these annotations (such as callSuper, includeFieldNames and exclude) cannot be set with @Data. If you need to set non-default values for any of these parameters, just add those annotations explicitly; @Data is smart enough to defer to those annotations.*/
@AllArgsConstructor //
@Entity //now we Need a primary key
public class Weather {
   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String latitude;
    private String longitude;

    @Embedded
    private Weatherx weatherx; //the variable name should be same as the name of the json property

    public Weather() {}
}
