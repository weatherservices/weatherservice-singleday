package com.test.weather.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Embeddable;

@Data
@AllArgsConstructor
@Embeddable
public class Weatherx {

    private String temperatureC;
    private String temperatureF;
    private String precipitation;
    private String summary;

    public Weatherx() {}
}
