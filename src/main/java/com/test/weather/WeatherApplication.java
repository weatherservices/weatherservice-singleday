package com.test.weather;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.weather.model.Weather;
import com.test.weather.service.WeatherService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@SpringBootApplication
public class WeatherApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeatherApplication.class, args);
    }

    @Bean
    CommandLineRunner runner(WeatherService weatherService) {
        return args -> {
            // read JSON and load json
            ObjectMapper mapper = new ObjectMapper();
            TypeReference<List<Weather>> typeReference = new TypeReference<List<Weather>>() {
            };
            InputStream inputStream = TypeReference.class.getResourceAsStream("/json/weather.json");
            try {
                List<Weather> weathers = mapper.readValue(inputStream, typeReference);
                weatherService.save(weathers);
                System.out.println("Working..!");
            } catch (IOException e) {
                System.out.println("Unable to save weather data: " + e.getMessage());
            }

        };
    }
}
