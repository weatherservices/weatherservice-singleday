package com.test.weather.repository;

import com.test.weather.model.Weather;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

//just a repository to store and perform crud operation on the data stored inside it
public interface WeatherRepository extends CrudRepository<Weather,Long> {
    public Optional<Weather> findById(Long Id);
    public Optional<Weather> findByLatitudeAndLongitude(String latitude,String longitude);
}
