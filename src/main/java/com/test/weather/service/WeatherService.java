package com.test.weather.service;

import com.test.weather.model.Weather;
import com.test.weather.repository.WeatherRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@AllArgsConstructor
public class WeatherService {

    private final WeatherRepository weatherRepository;//needs to be initialised before usage -> in cons definitely, also needs data inside -> saved by ajckson

//    public WeatherService(WeatherRepository weatherRepository) { --->as we have used @AllArgsConstructor
//        this.weatherRepository = weatherRepository;
//    }

    public Iterable<Weather> findAll() {
        return weatherRepository.findAll();
    }

    //will save a single weather
    public Weather save(Weather weather)
    {
        return weatherRepository.save(weather)  ;
    }

    //will save a list of weathers -> will be called in the starting only while fetching data from JSSON using JACKSON library
    public void save(List<Weather> weathers)
    {
        weatherRepository.saveAll(weathers);
    }

    //get all weather using lat and long
    public Weather getWeatherByll(String latitude,String longitude) {
        for (Weather w :
                weatherRepository.findAll()) {
            if (w.getLongitude().equals(longitude) && w.getLatitude().equals(latitude)){
                return w;
            }
        }
        return null;

    }

    public void update(Long id,Weather weather)
    {
        weatherRepository.save(weather);
    }

    public void delete(Long id)
    {
       Weather l = weatherRepository.findById(id).get();
        weatherRepository.delete(l);
    }


    //get all weather using id
    public Weather getWeatherById(Long Id) {
        return  weatherRepository.findById(Id).get();
    }

    public Weather getWeatherByLatAndLong(String latitude,String longitude) {
        return  weatherRepository.findByLatitudeAndLongitude(latitude,longitude).get();
    }

}
