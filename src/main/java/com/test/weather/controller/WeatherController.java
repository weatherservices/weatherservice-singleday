package com.test.weather.controller;

import com.test.weather.model.Weather;
import com.test.weather.service.WeatherService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/weathers")
@CrossOrigin(origins = "http://localhost:3000")
@AllArgsConstructor
public class WeatherController {
    private WeatherService weatherService;//needs to be initialised before usage -> in cons definitely

//    public WeatherController(WeatherService weatherService) { --->as we have used @AllArgsConstructor
//        this.weatherService = weatherService;
//    }

    @GetMapping("/all")
    public Iterable<Weather> findAll() {
        return weatherService.findAll();
    }

    @RequestMapping(value = "/latlongresult", method = RequestMethod.POST)
    public ResponseEntity<Object> getWeatherbyll(@RequestBody Weather weather) {
        Weather wresponse = weatherService.getWeatherByll(weather.getLatitude(),weather.getLongitude());
        return new ResponseEntity<>(wresponse.getWeatherx(), HttpStatus.CREATED);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<Object> createWeather(@RequestBody Weather weather) {
        System.out.println("--------------------------------------------------");
        System.out.println(weather);
        System.out.println("--------------------------------------------------");
        weatherService.save(weather);
        return new ResponseEntity<>("Weather data is created successfully", HttpStatus.CREATED);
    }


    @RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object>
    updateWeather(@PathVariable("id") Long id, @RequestBody Weather weather) {

        weatherService.update(id, weather);
        return new ResponseEntity<>("Weather data is updated successsfully", HttpStatus.OK);
    }



    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> delete(@PathVariable("id") String id) {
        weatherService.delete(Long.valueOf(id));
        return new ResponseEntity<>("Weather data of given id is deleted successsfully", HttpStatus.OK);
    }


    //using -- derived queries--------
    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
    public ResponseEntity<Object> findById(@PathVariable("id") Long id) {
        return new ResponseEntity<>(weatherService.getWeatherById(id), HttpStatus.OK);
    }

    @RequestMapping(value = "/latandlong", method = RequestMethod.POST)
    public ResponseEntity<Object> getWeatherbylatAndlong(@RequestBody Weather weather) {
        Weather wresponse = weatherService.getWeatherByLatAndLong(weather.getLatitude(),weather.getLongitude());
        return new ResponseEntity<>(wresponse.getWeatherx(), HttpStatus.CREATED);
    }



}
